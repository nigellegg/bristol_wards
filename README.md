# README #


I downloaded the dataset of wards from the Bristol Open Data site – https://opendata.bristol.gov.uk/explore/dataset/wards-may-2016/.

I put the geo_shape data for the first ward, Frome Vale, into leaflet as a polygon, and nothing showed, though there were no errors.  So I zoomed out on the map, and, eventually, discovered Frome Vale, in the Indian Ocean, somewhere between Mogadishu and the Seychelles.

The Lat – Long pairs have been reversed on the website.  Oddly, the new open data patform seems to work fine with Long – Lat pairs – the previews of the ward boundaries are in Bristol.

I have been going through the laborious process of manually correcting this, but have completed it with a short script.  

The data is in ward named html and js files.  The html files use leaflet.js to show the polygon of the boundaries for that ward.  The js files are imported into bristol.html to show the map of all the wards in Bristol.    

Please download and use these files in your own projects. 

If you would like to contribute, please contact nigel@knowtext.co.uk
